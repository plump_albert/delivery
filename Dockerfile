FROM python:3.10-alpine
COPY . /home/app
RUN chmod 755 /home/app/docker-entrypoint.sh
WORKDIR /home/app
RUN pip3 install -r requirements.txt
ENTRYPOINT [ "python" ]
CMD [ "app.py" ]
