import flask
import database.init
from database.order import Order
import bus
import os
from dotenv import load_dotenv


load_dotenv()
app = flask.Flask(__name__,
                  static_folder='static',
                  static_url_path='',
                  template_folder='templates')


@app.route('/', defaults={'path': 'index.html'})
@app.route('/<path:path>')
def index(path):
    return app.send_static_file(path)


@app.route('/view')
def view():
    shipment_id = flask.request.args.get('shipment')
    order = Order.where('id', shipment_id)
    if not order:
        flask.abort(404)
    return flask.templating.render_template('shipment.html', context=order)


@app.errorhandler(404)
def not_found(_):
    return app.send_static_file('404.html')


amqp_url = 'amqp://%s:%s@%s:%s/%%2F' % (
    os.getenv('RABBITMQ_USER'),
    os.getenv('RABBITMQ_PASSWORD'),
    os.getenv('RABBITMQ_HOST'),
    os.getenv('RABBITMQ_PORT'),
)
consumer = bus.ExampleConsumer(amqp_url)
consumer.run()
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=int(os.getenv('APP_PORT') or 3000))
    consumer.stop()
