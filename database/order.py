from . import conn
from psycopg2.extras import DictCursor
from psycopg2 import sql


class Order(object):
    TABLENAME = sql.Identifier('orders')
    COLUMNS = ('recipient', 'address', 'email', 'store_id')

    def __init__(
            self, recipient=None, address=None, email=None, store_id=None,
            id=None):
        # Data schema
        self.id = id
        self.recipient = recipient
        self.address = address
        self.email = email
        self.store_id = store_id

    @staticmethod
    def all():
        with conn.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute("SELECT * from orders;")
            result = []
            for row in cursor:
                result.append(Order(row['recipient'], row['address'],
                                    row['email'], row['store_id'], row['id']))
        return result

    @staticmethod
    def where(column, value, operator='='):
        with conn.cursor(cursor_factory=DictCursor) as cursor:
            query = sql.SQL(f'SELECT * FROM orders WHERE \
            {column}{operator}{value};')
            cursor.execute(query)
            result = []
            for row in cursor:
                result.append(Order(row['recipient'], row['address'],
                                    row['email'], row['store_id'], row['id']))
        return result

    def save(self):
        with conn.cursor(cursor_factory=DictCursor) as cursor:
            values = (self.recipient,
                      self.address,
                      self.email,
                      self.store_id)
            query = sql.SQL('INSERT INTO {} ({}) VALUES ({}) RETURNING id;').format(
                Order.TABLENAME,
                sql.SQL(',').join(map(sql.Identifier, Order.COLUMNS)),
                sql.SQL(',').join(map(sql.Literal, values))
            )
            cursor.execute(query, {})
            self.id = cursor.fetchone()['id']
            conn.commit()
