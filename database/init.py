import os
from . import conn

with open(os.path.join(os.path.dirname(__file__), 'init.sql'), 'r') as sql:
    with conn.cursor() as cursor:
        cursor.execute(sql.read())
    conn.commit()
